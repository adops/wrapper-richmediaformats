import richMediaManager from '../../src/_richMediaManager.js';
import { logMessage, logError, safeJSONParse, deepClone } from '../../src/utils.js';
const config = require('../../src/site/config.json');

/* Name/Type of RichMedia Format*/
const NAME = 'adhesionNew';

/* Default Options */
const OPTIONS = {
                	"closeButton" : "true",
                  "position" : "footer",
                  "bottomPosition":"0",
                  "topPosition":"0",
                  "autoClose": "0",
                  "closePermanent" : "false"
                }

// Add banner pos
/*
    0 - Unknown
    1 - ATF
    2 - DEPRECATED
    3 - BTF
    4 - Header (for in-app)
    5 - Footer (for in-app)
    6 - Sidebar (for in-app)
    7 - Full Screen (for in-app)
*/
const BANNER_POS = "1";

/* 
	#Inititate ad format 
	This function will be run after ads are processed on page, Here you can do mutation and changes to the element
	Parameters
		el : DOM Element
*/
const initFunc = function(el){
	// If element doesn't exist do nothing
	if(!el) return;
	// If element doesn't have an ID, do nothing
	if(!el.id) return;

	// Move element as child of the body
	if(document.body && document.body != el.parentNode) {
        logMessage('__ RichMediaFormat | '+NAME+' : Moved to body element:',el.id);
        document.body.appendChild(el);
   }

   // Initiate this element as hidden, will be shown only when ready
   el.style.display = 'none';

   return true;
}

/*
	# Render Function
	This will called when the function should render
	Parameters:
		el : DOM element
		size : array of size ([w,h])
*/
const render = function(el,size){
	// Set options to default initially
	let options = deepClone(OPTIONS);

	// Get RichMedia data from data attributes
	let richMediaData = safeJSONParse(el.dataset.aaRichMediaFormat);

	if(!richMediaData.type){
		logError('__ RichMediaFormat | '+NAME+' : Error, type is not set');
		return false;
	}
	if(richMediaData.type != NAME){
		logError('__ RichMediaFormat | '+NAME+' : Error, type not the same as RichMedia Name');
		return false;
	}

	// Set options
	if(richMediaData.options){
		let optionsKeys = Object.keys(richMediaData.options);
		optionsKeys.forEach(option=>{
			options[option] = richMediaData.options[option];
			logMessage('__ RichMediaFormat | '+NAME+' : Setting '+option+' to value:',options[option]);
		});
	}    

  logMessage('__ RichMediaFormat | '+NAME+' : Rendering ',NAME,el,size,options);

  let clientWidth = window.screen.width || window.innerWidth && document.documentElement.clientWidth ? Math.min(window.innerWidth, document.documentElement.clientWidth) : window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;

  let width = size[0];
  let height = size[1];

  // make the element floating
  el.style.marginLeft = '0';
  el.style.paddingLeft = '0';
  el.style.width = width+'px';
  el.style.height = height+'px';
  el.style.position = 'fixed';
  el.style.left = ((clientWidth-width)/2)+'px';
  el.style.zIndex = '2147483640';
  if(options.position == 'header') el.style.top = options.topPosition;
  else el.style.bottom = options.bottomPosition;
  el.style.display = '';

  if(options.closeButton == "true"){
    // close button image
    var closeLink = window.document.createElement('a');
    //closeLink.href = '#';
    closeLink.innerHTML = 'X';
    closeLink.onclick = function(){
        el.style.display = 'none';
        el.children[1].style.display = 'none';
        el.children[1].innerHTML = '';
        logMessage('__ RichMediaFormat | '+NAME+' : Closing adunit ');
        if(options.closePermanent && options.closePermanent == 'true'){
          logMessage('__ RichMediaFormat | '+NAME+' : Closing permanent, destroying adunit');
          $$PREBID_GLOBAL$$.destroy([el]);
        }
    };
    closeLink.border = '0';
    closeLink.style.cssText = 'position:absolute; right:-1px; top:-8px; color:#fff; cursor: pointer; text-decoration:none;text-shadow:0 0 3px #000, 0 0 3px #000, 0 0 3px #000, 0 0 3px #000; font-size:14px; padding:4px; display:block;z-index:2147483646;';
    el.appendChild(closeLink);
  }

  options.autoClose = parseInt(options.autoClose);
  if(options.autoClose > 0){
        setTimeout(()=>{
          el.style.display = 'none';
          el.children[1].style.display = 'none';
          el.children[1].innerHTML = '';
          logMessage('__ RichMediaFormat | '+NAME+' : Auto-Closing adunit ');              
        },options.autoClose*1000);    
  }

  return true;
}

/*
	# Register Listener
	It will register a function into the render listener (GAM)
	Parameters
		el : DOM Element
		event : GAM slotRenderEnded event
*/
const registerListener = function(el,event){
	logMessage('__ RichMediaFormat | '+NAME+' : Register listener ',event);

	if(event.isEmpty){
		logMessage('__ RichMediaFormat | '+NAME+' : Response is Empty ');
		return false;
	}

	let targetingMap = event.slot.getTargetingMap();

	// Get size
	let size = event.size;

	logMessage('__ RichMediaFormat | '+NAME+' : Event Size is: ',size);

	if(size[0] == 1 && size[1] == 1){
		let hbSize = [];
		// Check for Prebid Size
		if(targetingMap[config.targetingPrefix+'_size'] && targetingMap[config.targetingPrefix+'_size'][0]){
			hbSize = targetingMap[config.targetingPrefix+'_size'][0].split('x');			
			logMessage('__ RichMediaFormat | '+NAME+' : Found Prebid size: ',hbSize);
		} else {
		  // Check for Amazon Size (targeting: amznsz)
		  if(targetingMap['amznsz'] && targetingMap['amznsz'][0]){
			hbSize = targetingMap['amznsz'][0].split('x');				
			logMessage('__ RichMediaFormat | '+NAME+' : Found Amazon size: ',hbSize);
		  }
		}
		// Check if any size was found
		if(hbSize[0] && hbSize[1]) {
		  size = [hbSize[0],hbSize[1]];
		  logMessage('__ RichMediaFormat | '+NAME+' : Using size: ',size);
		} else {
		  logMessage('__ RichMediaFormat | '+NAME+' : Cannot detect any size ',hbSize);
		}
	}  

  	if(size[0] == 1 && size[1] == 1){ 
        logError('__ RichMediaFormat | '+NAME+' : Error : Cannot detect the size of the adunit ',el.dataset.aaAdunit,el.id);
  	} else {
  		try{
  			render(el,size);
  		}catch(e){
  			logError('__ RichMediaFormat | '+NAME+' : Error: ',e)
  		}      	
  	}
  	return true;
}

/*
	RichMediaFormat element
*/
export const richMediaFormat = {
	"name" : NAME,
	"options" : OPTIONS,
	"bannerPos" : BANNER_POS,
	"initFunc" : initFunc,
	"registerListener" : registerListener
};

/*
	Add RichMediaFormat to RichMediaManager
*/
richMediaManager.addFormat(richMediaFormat);