import richMediaManager from '../../src/_richMediaManager.js';
import { logMessage, logError, safeJSONParse, deepClone } from '../../src/utils.js';
const config = require('../../src/site/config.json');

/* Name/Type of RichMedia Format*/
const NAME = 'stickyNew';

/* Default Options */
const OPTIONS = {
                	"closeButton" : "true",
                  "autoClose": "0",
                  "closePermanent" : "false"
                }

// Add banner pos
/*
    0 - Unknown
    1 - ATF
    2 - DEPRECATED
    3 - BTF
    4 - Header (for in-app)
    5 - Footer (for in-app)
    6 - Sidebar (for in-app)
    7 - Full Screen (for in-app)
*/
const BANNER_POS = "1";

/* 
	#Inititate ad format 
	This function will be run after ads are processed on page, Here you can do mutation and changes to the element
	Parameters
		el : DOM Element
*/
const initFunc = function(el){
	// If element doesn't exist do nothing
	if(!el) return;
	// If element doesn't have an ID, do nothing
	if(!el.id) return;

	// Move element as child of the body
	if(document.body && document.body != el.parentNode) {
        logMessage('__ RichMediaFormat | '+NAME+' : Moved to body element:',el.id);
        document.body.appendChild(el);
   }

   // Initiate this element as hidden, will be shown only when ready
   el.style.display = 'none';

   return true;
}

/*
	# Render Function
	This will called when the function should render
	Parameters:
		el : DOM element
*/
const render = function(el){
	// Set options to default initially
	let options = deepClone(OPTIONS);

	// Get RichMedia data from data attributes
	let richMediaData = safeJSONParse(el.dataset.aaRichMediaFormat);

	if(!richMediaData.type){
		logError('__ RichMediaFormat | '+NAME+' : Error, type is not set');
		return false;
	}
	if(richMediaData.type != NAME){
		logError('__ RichMediaFormat | '+NAME+' : Error, type not the same as RichMedia Name');
		return false;
	}

	// Set options
	if(richMediaData.options){
		let optionsKeys = Object.keys(richMediaData.options);
		optionsKeys.forEach(option=>{
			options[option] = richMediaData.options[option];
			logMessage('__ RichMediaFormat | '+NAME+' : Setting '+option+' to value:',options[option]);
		});
	}    

  logMessage('__ RichMediaFormat | '+NAME+' : Rendering ',NAME,el,options);

  // create container style
  var createContainerStyle = function()
  {
    let cssText = 'z-index:2147483640;position:fixed;';
    if(typeof options.top != 'undefined') cssText = cssText + 'top:'+options.top+'px;';
    if(typeof options.bottom != 'undefined') cssText = cssText + 'bottom:'+options.bottom+'px;';
    if(typeof options.left != 'undefined') cssText = cssText + 'left:'+options.left+'px;';
    if(typeof options.right != 'undefined') cssText = cssText + 'right:'+options.right+'px;';

    if(el && el.style.cssText) return el.style.cssText+cssText;
    return cssText;
  }

  // make the element floating
  el.style.marginLeft = '0';
  el.style.paddingLeft = '0';
  el.style.cssText = createContainerStyle();
  // Display Element
  el.style.display = '';

  if(options.closeButton == "true"){
    // close button image
    var closeLink = window.document.createElement('a');
    //closeLink.href = '#';
    closeLink.innerHTML = 'X';
    closeLink.onclick = function(){
        el.style.display = 'none';
        el.children[1].style.display = 'none';
        el.children[1].innerHTML = '';
        logMessage('__ RichMediaFormat | '+NAME+' : Closing adunit ');
        if(options.closePermanent && options.closePermanent == 'true'){
          logMessage('__ RichMediaFormat | '+NAME+' : Closing permanent, destroying adunit');
          $$PREBID_GLOBAL$$.destroy([el]);
        }
    };
    closeLink.border = '0';
    closeLink.style.cssText = 'position:absolute; right:-1px; top:-2px; color:#fff; cursor: pointer; text-decoration:none;text-shadow:0 0 3px #000, 0 0 3px #000, 0 0 3px #000, 0 0 3px #000; font-size:14px; padding:4px; display:block;z-index:2147483646;';
    el.appendChild(closeLink);
  }

  options.autoClose = parseInt(options.autoClose);
  if(options.autoClose > 0){
        setTimeout(()=>{
          el.style.display = 'none';
          el.children[1].style.display = 'none';
          el.children[1].innerHTML = '';
          logMessage('__ RichMediaFormat | '+NAME+' : Auto-Closing adunit ');              
        },options.autoClose*1000);    
  }

  return true;
}

/*
	# Register Listener
	It will register a function into the render listener (GAM)
	Parameters
		el : DOM Element
		event : GAM slotRenderEnded event
*/
const registerListener = function(el,event){
	logMessage('__ RichMediaFormat | '+NAME+' : Register listener ',event);

	if(event.isEmpty){
		logMessage('__ RichMediaFormat | '+NAME+' : Response is Empty ');
		return false;
	}

	try{
		render(el);
	}catch(e){
		logError('__ RichMediaFormat | '+NAME+' : Error: ',e)
	}

  return true;
}

/*
	RichMediaFormat element
*/
export const richMediaFormat = {
	"name" : NAME,
	"options" : OPTIONS,
	"bannerPos" : BANNER_POS,
	"initFunc" : initFunc,
	"registerListener" : registerListener
};

/*
	Add RichMediaFormat to RichMediaManager
*/
richMediaManager.addFormat(richMediaFormat);