import richMediaManager from '../../src/_richMediaManager.js';
import { logMessage, logError, safeJSONParse, deepClone } from '../../src/utils.js';
import { getSesDepth,getDevice } from '../../src/_utils.js';
const config = require('../../src/site/config.json');
const internalAdunits = require('../../src/site/adunits.json');

/* Name/Type of RichMedia Format*/
const NAME = 'interstitialNew';

/* Default Options */
const OPTIONS = {
                	"timeOpen" : 15,
                  "timeOpenShow" : true,
                  "maxDisplayPerSession":2000,
                  "pageviewDisplayStart":1,
                  "pageviewDisplayEvery": 2,
                  "backgroundColor" : "#000",
                  "opacity" : 0.8
                }

// Add banner pos
/*
    0 - Unknown
    1 - ATF
    2 - DEPRECATED
    3 - BTF
    4 - Header (for in-app)
    5 - Footer (for in-app)
    6 - Sidebar (for in-app)
    7 - Full Screen (for in-app)
*/
const BANNER_POS = "1";

/*
	Global variables
*/
let adunit,options,timeOpen,timer,counter,cover,header,intbody,creativeContainer,adClose,adunitEl;

let sesDepth = getSesDepth();

/* Storage key name*/
const STORAGE_NAME = '_$$PREBID_GLOBAL$$_'+NAME+'_fq';

/* 
	#Inititate ad format 
	This function will be run after ads are processed on page, Here you can do mutation and changes to the element
	Parameters
		el : DOM Element
*/
const initFunc = function(el){
	// If element doesn't exist do nothing
	if(!el) return;
	// If element doesn't have an ID, do nothing
	if(!el.id) return;

  return true;
}

/*
	Execute when page is loaded
	- Check if there is any interstitial
	- Set options
	- Key Interstitial Frequency Capping
*/
$$PREBID_GLOBAL$$.cmd.push(()=>{
	window.docReady(function(){
	  // Check if there is any Interstitial adunit
	  let interstitialAdunits = internalAdunits.filter((au)=>{
	    if(au.device.indexOf(getDevice()) < 0) return false; // Check if the devices matches
	    if(au.richMediaFormat && au.richMediaFormat.type && au.richMediaFormat.type == NAME) return true; // Check if interstitial is set
	    else return false;
	  });
	  if(interstitialAdunits.length == 0){
	    logMessage('__ RichMediaFormat | '+NAME+' : None setup');
	    return false;
	  } else if(interstitialAdunits.length > 1){
	    logMessage('__ RichMediaFormat | '+NAME+' : Multiple interstitial Adunits found, using first');
	  }
	  adunit = deepClone(interstitialAdunits[0]);

	  // Set Options
	  options = deepClone(OPTIONS); // Set Defaults

		// Set options
		if(adunit.richMediaFormat.options){
			let optionsKeys = Object.keys(adunit.richMediaFormat.options);
			optionsKeys.forEach(option=>{
				options[option] = adunit.richMediaFormat.options[option];
				logMessage('__ RichMediaFormat | '+NAME+' : Setting '+option+' to value:',options[option]);
			});
		}  
  
  	timeOpen = parseInt(options.timeOpen);
  	if(timeOpen <= 0) options.timeOpenShow = false;

		// Frequancy Capping
		let fq = window.sessionStorage.getItem(STORAGE_NAME);
		if(!fq) fq = '0';
		fq = parseInt(fq);

		// Check if exceeded maxDisplayPerSession
		if(fq >= options.maxDisplayPerSession){
			logMessage('__ RichMediaFormat | '+NAME+' : maxDisplayPerSession of '+options.maxDisplayPerSession+' HIT, fq:'+fq);
			return false;
		}

		// Check for pageviewDisplayStart
		if(options.pageviewDisplayStart > sesDepth){
			logMessage('__ RichMediaFormat | '+NAME+' : pageviewDisplayStart of '+options.pageviewDisplayStart+' MISS, sesDepth:'+sesDepth);
			return false;			
		}

		// Check pageviewDisplayEvery
		let pageCount = Math.abs(sesDepth-options.pageviewDisplayStart)%options.pageviewDisplayEvery;
		if(pageCount != 0){
			logMessage('__ RichMediaFormat | '+NAME+' : pageviewDisplayEvery of '+options.pageviewDisplayEvery+' MISS, pageCount:'+pageCount);
			return false;					
		}

	  // Generate an Unique ID
	  let randomNumber = Math.floor(Math.random() * 100000000);

	  // Build Wrapper
	  let wrapper = document.createElement('div');
	  		wrapper.id = "adInt_id_"+randomNumber;
	  		wrapper.classList.add('ad-interstitial');
	  		wrapper.innerHTML =       '<div class="ad-interstitial-cover" id="adInt_cover_'+randomNumber+'"></div>' +
	      '<div class="ad-interstitial-inner">' +
	        '<div class="ad-interstitial-header shadow-narrow" id="adInt_header_'+randomNumber+'">' +
	          '<div class="ad-col ad-col-25">' +
	            '<div class="ad-logo"></div>' +
	          '</div>' +
	          '<div class="ad-col ad-col-50">' +
	            '<div class="ad-message">' +
	              '<span class="bold">Advertisement </span>' +
	                '<span' + (( !options.timeOpenShow ) ? ' style="display:none;"' : '') + '> <br>'+
	                'Page requested will appear in <span id="adInt_counter_'+randomNumber+'">' + options.timeOpen + '</span> seconds' +
	              '</span>'+
	            '</div>' +
	          '</div>' +
	          '<div class="ad-img-right ad-col ad-col-25">' +
	            '<div class="ad-close bold" id="adInt_adClose_'+randomNumber+'">' +
	              'CLOSE [X]' +
	            '</div>' +
	          '</div>' +
	        '</div>' +
	        '<div class="ad-interstitial-body" id="adInt_body_'+randomNumber+'">' +
	          '<div class="ad-interstitial-creative-container" id="adInt_creative_'+randomNumber+'"><!-- ADUNIT HERE --></div>' +
	        '</div>' +
	      '</div>';
	  // Insert Wrapper
	  document.body.appendChild(wrapper);

	  // Get elements
	  counter = document.getElementById("adInt_counter_"+randomNumber);
	  cover = document.getElementById("adInt_cover_"+randomNumber);
	  header = document.getElementById("adInt_header_"+randomNumber);
	  intbody = document.getElementById("adInt_body_"+randomNumber);
	  creativeContainer = document.getElementById("adInt_creative_"+randomNumber);
	  adClose = document.getElementById("adInt_adClose_"+randomNumber);

	  // Insert Styles
	  var css = 'html.has-interstitial body{width:100%;height:100%;overflow:hidden;margin:0;text-align:left!important}html.has-interstitial embed,html.has-interstitial object,html.has-interstitial select{visibility:hidden;display:none}html.has-interstitial .ad-interstitial{display:block}html.has-interstitial .ad-interstitial embed,html.has-interstitial .ad-interstitial object,html.has-interstitial .ad-interstitial select{visibility:visible;display:block}.ad-interstitial{display:none;width:100%;height:100%;z-index:2147483646;color:#000;font-size:10px;font-family:arial,sans-serif;margin:0;padding:0;line-height:1;position:fixed;top:0;left:0;text-align:center}.ad-interstitial .shadow-narrow{box-shadow:0 0 8px 2px #111;-webkit-box-shadow:0 0 8px 2px #111;-moz-box-shadow:0 0 8px 2px #111;-o-box-shadow:0 0 8px 2px #111}.ad-interstitial .shadow-wide{box-shadow:0 0 15px 3px #111;-webkit-box-shadow:0 0 15px 3px #111;-moz-box-shadow:0 0 15px 3px #111;-o-box-shadow:0 0 15px 3px #111}.ad-interstitial .ad-interstitial-cover,.ad-interstitial .ad-interstitial-inner{width:100%;height:100%;position:absolute;top:0;left:0}.ad-interstitial .ad-interstitial-cover{background-color:#fff;opacity:1;z-index:999999998}.ad-interstitial .ad-interstitial-inner{z-index:999999999;position:absolute}.ad-interstitial .ad-interstitial-header{height:40px;background-color:#000;color:#fff;padding:3px 8px;position:relative;z-index:5}.ad-interstitial .ad-interstitial-header .ad-logo{background-position:left center;background-repeat:no-repeat;height:40px}.ad-interstitial .ad-interstitial-header .ad-message{text-align:center;height:40px;line-height:20px}.ad-interstitial .ad-interstitial-header .ad-close{line-height:40px;cursor:pointer;text-align:right;float:right;display:block;height:40px}.ad-interstitial .ad-interstitial-body{position:relative;z-index:1}.ad-interstitial .ad-interstitial-body embed,.ad-interstitial .ad-interstitial-body iframe,.ad-interstitial .ad-interstitial-body img,.ad-interstitial .ad-interstitial-body object{margin:0 auto;border:0;outline:0;display:block}.ad-interstitial .ad-interstitial-body .ad-interstitial-creative-container{position:relative;margin:0 auto;display:inline-block}.ad-interstitial .ad-interstitial-body .ad-interstitial-creative-container a{display:inline-block}.ad-interstitial .ad-interstitial-body .ad-interstitial-click-overlay{background-color:transparent;z-index:9999999999;top:0;left:0;position:absolute;display:block;border:0;text-decoration:none}.ad-interstitial .ad-col{float:left;height:40px;overflow:hidden}.ad-interstitial .ad-col-25{width:25%}.ad-interstitial .ad-col-50{width:50%}.ad-interstitial .hidden{display:none}.ad-interstitial .bold{font-weight:700;font-size:12px}',
	  head = document.head || document.getElementsByTagName('head')[0],
	  style = document.createElement('style');

	  head.appendChild(style);

	  style.type = 'text/css';
	  if(style.styleSheet){
	    // This is required for IE8 and below.
	    style.styleSheet.cssText = css;
	  } else {
	    style.appendChild(document.createTextNode(css));
	  }

	  // Listen to click on close button  
	  adClose.addEventListener('click', function(){
	        close();
	  });

	  // Insert Adunit
	  adunitEl = document.createElement('div');
	  adunitEl.dataset.aaad = 'true';
	  adunitEl.dataset.aaAdunit = adunit.adunit;
	  adunitEl.dataset.aaInterstitial = 'true';

	  creativeContainer.appendChild(adunitEl); 

	  logMessage('__ RichMediaFormat | '+NAME+' : HTML injected into DOM');

		return true;
	});
})

/*
	Helper functions
*/
let startTimer = function(){
  timer = setTimeout(updateTimer,1000);
}

let clearTimer = function(){
  clearTimeout(timer);
}

let updateTimer = function(){
  timeOpen--;
  counter.innerHTML = timeOpen;  
  if(timeOpen){
      startTimer();
  } else {
      // close interstitial
      close();
  }
}

let close = function(){
  clearTimer();    
  
  document.documentElement.classList.remove('has-interstitial');

  $$PREBID_GLOBAL$$.destroyAdunits([adunit.adunit]);

  document.body.removeChild(wrapper);

  logMessage('__ RichMediaFormat | '+NAME+' : Closed');
}

/*
	# Render Function
	This will called when the function should render
	Parameters:
		el : DOM element
		size : array of size ([w,h])
*/
const render = function(el,size){
  let width = size[0];
  let height = size[1];

  // Check if all has been setup
  if(	
  		typeof counter == 'undefined' || 
  		typeof cover == 'undefined' || 
  		typeof header == 'undefined' || 
  		typeof intbody == 'undefined' || 
  		typeof creativeContainer == 'undefined' || 
  		typeof adClose == 'undefined' || 
  		typeof adunitEl == 'undefined'
  	){  	
		logError('__ RichMediaFormat | '+NAME+' : Not setup correctly, cannot render it ');
		return false;
  }

  // align creative
  var offset = ((window.screen.height - header.offsetHeight) - height * 2) / 2;
  creativeContainer.style.top = (offset > 0 ? offset : 0) + 'px';
  creativeContainer.style.width = width;

  // set background and opacity
  cover.style.backgroundColor = options.backgroundColor;
  cover.style.opacity = options.opacity;

  // add class to HTML
  document.documentElement.classList.add('has-interstitial');

  // Listen to click on close button  
  adClose.addEventListener('click', function(){
        close();
  });

  // Start Timer
  if(timeOpen > 0) startTimer();

  // Set Capping 
	let fq = window.sessionStorage.getItem(STORAGE_NAME);
	if(!fq) fq = '0';
	fq = (parseInt(fq) + 1).toString();
	window.sessionStorage.setItem(STORAGE_NAME,fq);

  return true;
}

/*
	# Register Listener
	It will register a function into the render listener (GAM)
	Parameters
		el : DOM Element
		event : GAM slotRenderEnded event
*/
const registerListener = function(el,event){
	logMessage('__ RichMediaFormat | '+NAME+' : Register listener ',event);

	if(event.isEmpty){
		logMessage('__ RichMediaFormat | '+NAME+' : Response is Empty ');
		return false;
	}

	let targetingMap = event.slot.getTargetingMap();

	// Get size
	let size = event.size;

	logMessage('__ RichMediaFormat | '+NAME+' : Event Size is: ',size);

	if(size[0] == 1 && size[1] == 1){
		let hbSize = [];
		// Check for Prebid Size
		if(targetingMap[config.targetingPrefix+'_size'] && targetingMap[config.targetingPrefix+'_size'][0]){
			hbSize = targetingMap[config.targetingPrefix+'_size'][0].split('x');			
			logMessage('__ RichMediaFormat | '+NAME+' : Found Prebid size: ',hbSize);
		} else {
		  // Check for Amazon Size (targeting: amznsz)
		  if(targetingMap['amznsz'] && targetingMap['amznsz'][0]){
			hbSize = targetingMap['amznsz'][0].split('x');				
			logMessage('__ RichMediaFormat | '+NAME+' : Found Amazon size: ',hbSize);
		  }
		}
		// Check if any size was found
		if(hbSize[0] && hbSize[1]) {
		  size = [hbSize[0],hbSize[1]];
		  logMessage('__ RichMediaFormat | '+NAME+' : Using size: ',size);
		} else {
		  logMessage('__ RichMediaFormat | '+NAME+' : Cannot detect any size ',hbSize);
		}
	}  

  	if(size[0] == 1 && size[1] == 1){ 
        logError('__ RichMediaFormat | '+NAME+' : Error : Cannot detect the size of the adunit ',el.dataset.aaAdunit,el.id);
  	} else {
  		try{
  			render(el,size);
  		}catch(e){
  			logError('__ RichMediaFormat | '+NAME+' : Error: ',e)
  		}      	
  	}
  	return true;
}

/*
	RichMediaFormat element
*/
export const richMediaFormat = {
	"name" : NAME,
	"options" : OPTIONS,
	"bannerPos" : BANNER_POS,
	"initFunc" : initFunc,
	"registerListener" : registerListener
};

/*
	Add RichMediaFormat to RichMediaManager
*/
richMediaManager.addFormat(richMediaFormat);