import richMediaManager from '../../src/_richMediaManager.js';
import { logMessage, logError, safeJSONParse, deepClone } from '../../src/utils.js';

/* Name/Type of RichMedia Format*/
const NAME = 'example';

/* Default Options */
const OPTIONS = {
                	"backgroundColor" : "black",
                	"textColor": "red"
                }

// Add banner pos
/*
    0 - Unknown
    1 - ATF
    2 - DEPRECATED
    3 - BTF
    4 - Header (for in-app)
    5 - Footer (for in-app)
    6 - Sidebar (for in-app)
    7 - Full Screen (for in-app)
*/
const BANNER_POS = "1";

/* 
	#Inititate ad format 
	This function will be run after ads are processed on page, Here you can do mutation and changes to the element
	Parameters
		el : DOM Element
*/
const initFunc = function(el){
	// If element doesn't exist do nothing
	if(!el) return;
	// If element doesn't have an ID, do nothing
	if(!el.id) return;

	// Nothing else to do, all OK
  return true;
}

/*
	# Render Function
	This will called when the function should render
	Parameters:
		el : DOM element
		text : the text to be displayed in the new div label
*/
const render = function(el,text){
	// Set options to default initially
	let options = deepClone(OPTIONS);

	// Get RichMedia data from data attributes
	let richMediaData = safeJSONParse(el.dataset.aaRichMediaFormat);

	if(!richMediaData.type){
		logError('__ RichMediaFormat | '+NAME+' : Error, type is not set');
		return false;
	}
	if(richMediaData.type != NAME){
		logError('__ RichMediaFormat | '+NAME+' : Error, type not the same as RichMedia Name');
		return false;
	}

	// Set options
	if(richMediaData.options){
		let optionsKeys = Object.keys(richMediaData.options);
		optionsKeys.forEach(option=>{
			options[option] = richMediaData.options[option];
			logMessage('__ RichMediaFormat | '+NAME+' : Setting '+option+' to value:',options[option]);
		});
	}    

  logMessage('__ RichMediaFormat | '+NAME+' : Rendering ',NAME,el,options);

	// Create a new DIV element
	let newDiv = document.createElement('div');

	// Set newDiv's ID to something that can be easily queried
	newDiv.id = el.id + '_label';

	// Hide this new div until adunit is rendered
	newDiv.style.display = 'none';

	// Style new div to float inside the parent element at the top
	newDiv.style.position = 'absolute';
	newDiv.style.top = '0px';
	newDiv.style.left = '0px';

	// Add it as a child of el
	el.appendChild(newDiv);

  // Set the background color
  newDiv.style.backgroundColor = options.backgroundColor;

  // Set the text color
  newDiv.style.color = options.textColor;

  // Set the text
  newDiv.innerText = text;

  // Display the new div
  newDiv.style.display = '';

  return true;
}

/*
	# Register Listener
	It will register a function into the render listener (GAM)
	Parameters
		el : DOM Element
		event : GAM slotRenderEnded event
*/
const registerListener = function(el,event){
	logMessage('__ RichMediaFormat | '+NAME+' : Register listener ',event);

	if(event.isEmpty){
		logMessage('__ RichMediaFormat | '+NAME+' : Response is Empty ');
		return false;
	}

  // Set the new div label text to the adunit name
  let text = 'Adunit: '+ event.slot.getAdUnitPath();

  // If all ok, render 
  render(el,text);

  return true;
}

/*
	RichMediaFormat element
*/
export const richMediaFormat = {
	"name" : NAME,
	"options" : OPTIONS,
	"bannerPos" : BANNER_POS,
	"initFunc" : initFunc,
	"registerListener" : registerListener
};

/*
	Add RichMediaFormat to RichMediaManager
*/
richMediaManager.addFormat(richMediaFormat);