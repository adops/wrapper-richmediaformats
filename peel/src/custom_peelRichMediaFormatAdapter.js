import richMediaManager from '../../src/_richMediaManager.js';
import { logMessage, logError, safeJSONParse, deepClone } from '../../src/utils.js';
const config = require('../../src/site/config.json');

/* Name/Type of RichMedia Format*/
const NAME = 'peelNew';

/* Default Options */
const OPTIONS = {
                	"closeButton" : "true",
                	"top" : "0",
                	"right" : "0",
                  "autoClose": "0",
                  "closePermanent" : "false"
                }

// Add banner pos
/*
    0 - Unknown
    1 - ATF
    2 - DEPRECATED
    3 - BTF
    4 - Header (for in-app)
    5 - Footer (for in-app)
    6 - Sidebar (for in-app)
    7 - Full Screen (for in-app)
*/
const BANNER_POS = "1";

/* 
	#Inititate ad format 
	This function will be run after ads are processed on page, Here you can do mutation and changes to the element
	Parameters
		el : DOM Element
*/
const initFunc = function(el){
	// If element doesn't exist do nothing
	if(!el) return;
	// If element doesn't have an ID, do nothing
	if(!el.id) return;

	// Move element as child of the body
	if(document.body && document.body != el.parentNode) {
        logMessage('__ RichMediaFormat | '+NAME+' : Moved to body element:',el.id);
        document.body.appendChild(el);
   }

   // Initiate this element as hidden, will be shown only when ready
   el.style.display = 'none';

   return true;
}

/*
	# Render Function
	This will called when the function should render
	Parameters:
		el : DOM element
*/
const render = function(el){
	// Set options to default initially
	let options = deepClone(OPTIONS);

	// Get RichMedia data from data attributes
	let richMediaData = safeJSONParse(el.dataset.aaRichMediaFormat);

	if(!richMediaData.type){
		logError('__ RichMediaFormat | '+NAME+' : Error, type is not set');
		return false;
	}
	if(richMediaData.type != NAME){
		logError('__ RichMediaFormat | '+NAME+' : Error, type not the same as RichMedia Name');
		return false;
	}

	// Set options
	if(richMediaData.options){
		let optionsKeys = Object.keys(richMediaData.options);
		optionsKeys.forEach(option=>{
			options[option] = richMediaData.options[option];
			logMessage('__ RichMediaFormat | '+NAME+' : Setting '+option+' to value:',options[option]);
		});
	}    

  logMessage('__ RichMediaFormat | '+NAME+' : Rendering ',NAME,el,options);

  // Insert Styles
  var css = '.peelexpand{-webkit-clip-path: circle(100%) !important;clip-path: circle(100%) !important;}',
  head = document.head || document.getElementsByTagName('head')[0],
  style = document.createElement('style');

  head.appendChild(style);

  style.type = 'text/css';
  if(style.styleSheet){
    // This is required for IE8 and below.
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }  

  // create container style
  var createContainerStyle = function()
  {
    // Establish placement in page
    let placementInPage = 'top-right';
    if(typeof options.top != 'undefined' && typeof options.left != 'undefined') placementInPage = 'top-left';
    if(typeof options.bottom != 'undefined' && typeof options.right != 'undefined') placementInPage = 'bottom-right';
    if(typeof options.bottom != 'undefined' && typeof options.left != 'undefined') placementInPage = 'bottom-left';

    // Based on placement in page, define the clip path
    let clipPath = 'circle(227px at 320px 0%);'; // top-right
    if(placementInPage == 'top-left') clipPath = 'circle(260px at 40px -10%)'; // top-left
    if(placementInPage == 'bottom-right') clipPath = 'circle(227px at 320px 90%)'; // bottom-right
    if(placementInPage == 'bottom-left') clipPath = 'circle(220px at 20px 90%)'; // bottom-left

    // Construct CSS for element
    let cssText = 'z-index:2147483640;position:fixed;'; // float

    // Position
    if(typeof options.top != 'undefined') cssText = cssText + 'top:'+options.top+'px;';
    if(typeof options.bottom != 'undefined') cssText = cssText + 'bottom:'+options.bottom+'px;';
    if(typeof options.left != 'undefined') cssText = cssText + 'left:'+options.left+'px;';
    if(typeof options.right != 'undefined') cssText = cssText + 'right:'+options.right+'px;';

    // Clip
    cssText = cssText + 'clip-path: '+clipPath+';';
    cssText = cssText + '-webkit-clip-path: '+clipPath+';';

    // Transition
    cssText = cssText + '-webkit-transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94);';
    cssText = cssText + '-webkit-transition-delay: !important;';
    cssText = cssText + '-moz-transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) !important;';
    cssText = cssText + '-o-transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) !important;';
    cssText = cssText + 'transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) !important;  ';

    if(el && el.style.cssText) return el.style.cssText+cssText;
    return cssText;
  }

  // make the element floating
  el.style.marginLeft = '0';
  el.style.paddingLeft = '0';
  el.style.cssText = createContainerStyle();
  // Display Element
  el.style.display = '';

  // peel expand functionality
  el.addEventListener('mouseenter', e => {
    el.classList.add("peelexpand");
  });
  el.addEventListener('mouseleave', e => {
    el.classList.remove("peelexpand");
  });
  // For mobile
  document.body.addEventListener('touchstart', e => {
    el.classList.add("peelexpand");
  });
  // Disable closing expand on touch end
  /*document.body.addEventListener('touchend', e => {
    el.classList.remove("peelexpand");
  });*/

  if(options.closeButton == "true"){
    // close button image
    var closeLink = window.document.createElement('a');
    //closeLink.href = '#';
    closeLink.innerHTML = 'X';
    closeLink.onclick = function(){
        el.style.display = 'none';
        el.children[1].style.display = 'none';
        el.children[1].innerHTML = '';
        logMessage('__ RichMediaFormat | '+NAME+' : Closing adunit ');
        if(options.closePermanent && options.closePermanent == 'true'){
          logMessage('__ RichMediaFormat | '+NAME+' : Closing permanent, destroying adunit');
          $$PREBID_GLOBAL$$.destroy([el]);
        }
    };
    closeLink.border = '0';
      closeLink.style.cssText = 'position:absolute; right:-2px; top:-5px; color:#fff; cursor: pointer; text-decoration:none;text-shadow:0 0 3px #000, 0 0 3px #000, 0 0 3px #000, 0 0 3px #000; font-size:14px; padding:4px; display:block;z-index:2147483646;';
    el.appendChild(closeLink);
  }

  options.autoClose = parseInt(options.autoClose);
  if(options.autoClose > 0){
        setTimeout(()=>{
          el.style.display = 'none';
          el.children[1].style.display = 'none';
          el.children[1].innerHTML = '';
          logMessage('__ RichMediaFormat | '+NAME+' : Auto-Closing adunit ');              
        },options.autoClose*1000);    
  }

  return true;
}

/*
	# Register Listener
	It will register a function into the render listener (GAM)
	Parameters
		el : DOM Element
		event : GAM slotRenderEnded event
*/
const registerListener = function(el,event){
	logMessage('__ RichMediaFormat | '+NAME+' : Register listener ',event);

	if(event.isEmpty){
		logMessage('__ RichMediaFormat | '+NAME+' : Response is Empty ');
    return false;
	}

	try{
		render(el);
	}catch(e){
		logError('__ RichMediaFormat | '+NAME+' : Error: ',e)
	}

  return true;
}

/*
	RichMediaFormat element
*/
export const richMediaFormat = {
	"name" : NAME,
	"options" : OPTIONS,
	"bannerPos" : BANNER_POS,
	"initFunc" : initFunc,
	"registerListener" : registerListener
};

/*
	Add RichMediaFormat to RichMediaManager
*/
richMediaManager.addFormat(richMediaFormat);